<?php
return [
    'tab_advanced_links'=> 'Liens (Avancé)',
    'tab_display'=> 'Affichage',
    'tab_menu'=> 'Menu',
    'hint_depth_header'=> 'Notez que les elements du header ne supportent que :depth niveaux',
    'hint_depth_footer'=> 'Notez que les elements du footer ne supportent que :depth niveaux',
    'hint_url'=> 'complète l\'url avec le domaine actuel du site',
    'hint_external_url'=>'URL externe, tel quel',
    'hint_name'=>'Nom du menu, affiché tel quel',

    'display_in_footer'=>'Afficher dans le footer',
    'display_in_header'=>'Afficher dans le header',

];