<?php

namespace KDA\Navigation\Admin\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Navigation extends \KDA\Navigation\Models\Navigation
{
    use CrudTrait;
    
}
