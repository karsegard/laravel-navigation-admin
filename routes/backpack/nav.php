<?php
Route::group([
    'namespace'  => 'KDA\Navigation\Admin\Http\Controllers\Admin',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware()],
], function () {
    if (config('kda.navadmin.setup_routes', true)) {

      
        Route::crud('navigation', 'NavigationCrudController');
    }
});
